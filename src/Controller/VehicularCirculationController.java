package Controller;

import Model.VehicularCirculationModel;
import View.VehicularCirculation;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;

public class VehicularCirculationController implements ActionListener {

    private VehicularCirculation VCF;
    private VehicularCirculationModel VCM;
    JFileChooser d;

    public VehicularCirculationController(VehicularCirculation VCF) {
        super();
        this.VCF = VCF;
        this.VCM = new VehicularCirculationModel();
        d = new JFileChooser();
    }

    public VehicularCirculationModel getVCM() {
        return VCM;
    }

    public void setVCM(VehicularCirculationModel VCM) {
        this.VCM = VCM;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        switch (e.getActionCommand()) {
            case "Save":
                Save();
                break;

            case "Clean":
                Clean();
                break;

            case "Select":
                Select();
                break;
        }
    }

    public void Save() {
        d.showSaveDialog(VCF);
        VCM = VCF.getData();
        WriteInfo(d.getSelectedFile());
    }

    public void Select() {
        d.showOpenDialog(VCF);
        VCM = readInfo(d.getSelectedFile());
        VCF.setData(VCM);
    }

    public void WriteInfo(File file) {
        try {
            ObjectOutputStream w = new ObjectOutputStream(new FileOutputStream(file));
            w.writeObject(getVCM());
            w.flush();
        } catch (IOException ex) {
            Logger.getLogger(VehicularCirculationController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private VehicularCirculationModel readInfo(File file) {
        try {
            ObjectInputStream ois = new ObjectInputStream(new FileInputStream(file));
            return (VehicularCirculationModel) ois.readObject();
        } catch (FileNotFoundException e) {
            JOptionPane.showMessageDialog(VCF, e.getMessage(), VCF.getTitle(), JOptionPane.WARNING_MESSAGE);
        } catch (IOException | ClassNotFoundException ex) {
            Logger.getLogger(VehicularCirculationController.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public void Clean() {
        VCF.clean();
    }
}
