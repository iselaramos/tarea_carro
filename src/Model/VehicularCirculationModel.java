package Model;

import java.io.Serializable;

public class VehicularCirculationModel implements Serializable {

    String Brand, Model, Color, Chassis, VIN, Placa, Gravamen, FuelType, TypeOfUse, Service,DateOfIssues;
    int Year, NumPass, Tonnage, NumCilinder;

    public VehicularCirculationModel(String DateOfIssues,String Brand, String Model, String Color, String Chassis, String VIN, String Placa, String Gravamen, String FuelType, String TypeOfUse, String Service, int Year, int NumPass, int Tonnage, int NumCilinder) {
        this.DateOfIssues = DateOfIssues;
        this.Brand = Brand;
        this.Model = Model;
        this.Color = Color;
        this.Chassis = Chassis;
        this.VIN = VIN;
        this.Placa = Placa;
        this.Gravamen = Gravamen;
        this.FuelType = FuelType;
        this.TypeOfUse = TypeOfUse;
        this.Service = Service;
        this.Year = Year;
        this.NumPass = NumPass;
        this.Tonnage = Tonnage;
        this.NumCilinder = NumCilinder;
    }

    public VehicularCirculationModel() {
        this.Brand = "";
        this.Model = "";
        this.Color = "";
        this.Chassis = "";
        this.VIN = "";
        this.Placa = "";
        this.Gravamen = "";
        this.FuelType = "";
        this.TypeOfUse = "";
        this.Service = "";
        this.Year = 0;
        this.NumPass = 0;
        this.Tonnage = 0;
        this.NumCilinder = 0;
        this.DateOfIssues = "";
    }
    
     public String getDateOfIssues() {
        return DateOfIssues;
    }

    public void setDateOfIssues(String DateOfIssues) {
        this.DateOfIssues = DateOfIssues;
    }

    public String getBrand() {
        return Brand;
    }

    public void setBrand(String Brand) {
        this.Brand = Brand;
    }

    public String getModel() {
        return Model;
    }

    public void setModel(String Model) {
        this.Model = Model;
    }

    public String getColor() {
        return Color;
    }

    public void setColor(String Color) {
        this.Color = Color;
    }

    public String getChassis() {
        return Chassis;
    }

    public void setChassis(String Chassis) {
        this.Chassis = Chassis;
    }

    public String getVIN() {
        return VIN;
    }

    public void setVIN(String VIN) {
        this.VIN = VIN;
    }

    public String getPlaca() {
        return Placa;
    }

    public void setPlaca(String Placa) {
        this.Placa = Placa;
    }

    public String getGravamen() {
        return Gravamen;
    }

    public void setGravamen(String Gravamen) {
        this.Gravamen = Gravamen;
    }

    public String getFuelType() {
        return FuelType;
    }

    public void setFuelType(String FuelType) {
        this.FuelType = FuelType;
    }

    public String getTypeOfUse() {
        return TypeOfUse;
    }

    public void setTypeOfUse(String TypeOfUse) {
        this.TypeOfUse = TypeOfUse;
    }

    public String getService() {
        return Service;
    }

    public void setService(String Service) {
        this.Service = Service;
    }

    public int getYear() {
        return Year;
    }

    public void setYear(int Year) {
        this.Year = Year;
    }

    public int getNumPass() {
        return NumPass;
    }

    public void setNumPass(int NumPass) {
        this.NumPass = NumPass;
    }

    public int getTonnage() {
        return Tonnage;
    }

    public void setTonnage(int Tonnage) {
        this.Tonnage = Tonnage;
    }

    public int getNumCilinder() {
        return NumCilinder;
    }

    public void setNumCilinder(int NumCilinder) {
        this.NumCilinder = NumCilinder;
    }

}
