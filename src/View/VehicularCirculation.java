package View;

import Controller.VehicularCirculationController;
import Model.VehicularCirculationModel;
import java.awt.Cursor;
import java.awt.Toolkit;
import javax.swing.DefaultComboBoxModel;

public class VehicularCirculation extends javax.swing.JFrame {

    public VehicularCirculation() {
        initComponents();
        Charge();
    }

    public void Charge() {
        this.setTitle("\t\tVehicular Circulation");
        this.setLocationRelativeTo(this);
        this.setResizable(false);
        setController();
        setGravamenModel();
        setFuelTypeModel();
        setTypeOfUseModel();
        setServiceModel();
        SaveButton.setCursor(new Cursor(HAND_CURSOR));
        CleanButton.setCursor(new Cursor(HAND_CURSOR));
        SelectButton.setCursor(new Cursor(HAND_CURSOR));
    }

    private void setGravamenModel() {
        DefaultComboBoxModel GravamenModel = new DefaultComboBoxModel<>();
        GravamenModel.addElement("Yes");
        GravamenModel.addElement("No");
        GravamenComboBox.setModel(GravamenModel);
        GravamenComboBox.setSelectedItem(null);
    }

    private void setFuelTypeModel() {
        DefaultComboBoxModel FuelTypeModel = new DefaultComboBoxModel<>();
        FuelTypeModel.addElement("Gasoline");
        FuelTypeModel.addElement("Diesel");
        FuelTypeComboBox.setModel(FuelTypeModel);
        FuelTypeComboBox.setSelectedItem(null);
    }

    private void setTypeOfUseModel() {
        DefaultComboBoxModel TypeOfUseModel = new DefaultComboBoxModel<>();
        TypeOfUseModel.addElement("Particular");
        TypeOfUseModel.addElement("Taxi");
        TypeOfUseModel.addElement("Bus");
        TypeOfUseComboBox.setModel(TypeOfUseModel);
        TypeOfUseComboBox.setSelectedItem(null);
    }

    private void setServiceModel() {
        DefaultComboBoxModel ServiceModel = new DefaultComboBoxModel<>();
        ServiceModel.addElement("Particular");
        ServiceModel.addElement("Private");
        ServiceComboBox.setModel(ServiceModel);
        ServiceComboBox.setSelectedItem(null);
    }

    public void clean() {
        this.BrandTextField.setText("");
        this.ModelTextField.setText("");
        this.YearTextField.setText("");
        this.ColorTextField.setText("");
        this.ChassisTextField.setText("");
        this.VinTextField.setText("");
        this.PlacaTextField.setText("");
        this.NumPassTextField.setText("");
        this.TonnageTextField.setText("");
        this.NumCilinderTextField.setText("");
        this.DateOfIssuesTextField.setText("");
        this.GravamenComboBox.setSelectedItem(null);
        this.FuelTypeComboBox.setSelectedItem(null);
        this.TypeOfUseComboBox.setSelectedItem(null);
        this.ServiceComboBox.setSelectedItem(null);
    }

    public void setController() {
        VehicularCirculationController VCC = new VehicularCirculationController(this);
        SaveButton.addActionListener(VCC);
        CleanButton.addActionListener(VCC);
        SelectButton.addActionListener(VCC);
    }

    public VehicularCirculationModel getData() {
        VehicularCirculationModel VCM = new VehicularCirculationModel();

        VCM.setBrand(BrandTextField.getText());
        VCM.setModel(ModelTextField.getText());
        VCM.setYear(Integer.parseInt(YearTextField.getText()));
        VCM.setColor(ColorTextField.getText());
        VCM.setChassis(ChassisTextField.getText());
        VCM.setVIN(VinTextField.getText());
        VCM.setPlaca(PlacaTextField.getText());
        VCM.setGravamen(GravamenComboBox.getSelectedItem().toString());
        VCM.setNumPass(Integer.parseInt(NumPassTextField.getText()));
        VCM.setTonnage(Integer.parseInt(TonnageTextField.getText()));
        VCM.setNumCilinder(Integer.parseInt(NumCilinderTextField.getText()));
        VCM.setFuelType(FuelTypeComboBox.getSelectedItem().toString());
        VCM.setTypeOfUse(TypeOfUseComboBox.getSelectedItem().toString());
        VCM.setService(ServiceComboBox.getSelectedItem().toString());
        VCM.setDateOfIssues(DateOfIssuesTextField.getText());

        return VCM;
    }

    public void setData(VehicularCirculationModel VCM) {
        BrandTextField.setText(VCM.getBrand());
        ModelTextField.setText(VCM.getModel());
        YearTextField.setText(String.valueOf(VCM.getYear()));
        ColorTextField.setText(VCM.getColor());
        ChassisTextField.setText(VCM.getChassis());
        VinTextField.setText(VCM.getVIN());
        PlacaTextField.setText(VCM.getPlaca());
        GravamenComboBox.setSelectedItem(VCM.getGravamen());
        NumPassTextField.setText(String.valueOf(VCM.getNumPass()));
        TonnageTextField.setText(String.valueOf(VCM.getTonnage()));
        NumCilinderTextField.setText(String.valueOf(VCM.getNumCilinder()));
        FuelTypeComboBox.setSelectedItem(VCM.getFuelType());
        TypeOfUseComboBox.setSelectedItem(VCM.getTypeOfUse());
        ServiceComboBox.setSelectedItem(VCM.getService());
        DateOfIssuesTextField.setText(VCM.getDateOfIssues());
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jPanel2 = new javax.swing.JPanel();
        jPanel3 = new javax.swing.JPanel();
        jLabel2 = new javax.swing.JLabel();
        BrandTextField = new javax.swing.JTextField();
        jLabel5 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        jLabel9 = new javax.swing.JLabel();
        jLabel10 = new javax.swing.JLabel();
        ModelTextField = new javax.swing.JTextField();
        YearTextField = new javax.swing.JTextField();
        ColorTextField = new javax.swing.JTextField();
        ChassisTextField = new javax.swing.JTextField();
        PlacaTextField = new javax.swing.JTextField();
        VinTextField = new javax.swing.JTextField();
        jLabel11 = new javax.swing.JLabel();
        jLabel12 = new javax.swing.JLabel();
        jLabel13 = new javax.swing.JLabel();
        jLabel14 = new javax.swing.JLabel();
        jLabel15 = new javax.swing.JLabel();
        jLabel16 = new javax.swing.JLabel();
        jLabel18 = new javax.swing.JLabel();
        NumPassTextField = new javax.swing.JTextField();
        TonnageTextField = new javax.swing.JTextField();
        NumCilinderTextField = new javax.swing.JTextField();
        GravamenComboBox = new javax.swing.JComboBox<>();
        TypeOfUseComboBox = new javax.swing.JComboBox<>();
        FuelTypeComboBox = new javax.swing.JComboBox<>();
        ServiceComboBox = new javax.swing.JComboBox<>();
        jLabel19 = new javax.swing.JLabel();
        DateOfIssuesTextField = new javax.swing.JFormattedTextField();
        SaveButton = new javax.swing.JButton();
        CleanButton = new javax.swing.JButton();
        SelectButton = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        getContentPane().setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jPanel1.setBackground(new java.awt.Color(153, 0, 255));
        jPanel1.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));
        jPanel1.setPreferredSize(new java.awt.Dimension(580, 70));

        jLabel1.setFont(new java.awt.Font("Constantia", 1, 18)); // NOI18N
        jLabel1.setForeground(new java.awt.Color(255, 255, 255));
        jLabel1.setText("Vehicular Circulation");

        jLabel3.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Resources/IconVehicle.png"))); // NOI18N

        jLabel4.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Resources/IconVehicle.png"))); // NOI18N

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(71, 71, 71)
                .addComponent(jLabel4)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel3)
                .addContainerGap(60, Short.MAX_VALUE))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(27, 27, 27)
                .addComponent(jLabel1)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel4, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, 73, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
        );

        getContentPane().add(jPanel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 590, 70));

        jPanel2.setBackground(new java.awt.Color(204, 204, 204));
        jPanel2.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        jPanel3.setBackground(new java.awt.Color(204, 204, 204));
        jPanel3.setBorder(javax.swing.BorderFactory.createTitledBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(41, 43, 45)), "Information", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Arial Black", 0, 12), new java.awt.Color(0, 0, 0))); // NOI18N

        jLabel2.setBackground(new java.awt.Color(0, 0, 0));
        jLabel2.setForeground(new java.awt.Color(0, 0, 0));
        jLabel2.setText("Brand");

        jLabel5.setForeground(new java.awt.Color(0, 0, 0));
        jLabel5.setText("Model");

        jLabel6.setForeground(new java.awt.Color(0, 0, 0));
        jLabel6.setText("Year");

        jLabel7.setForeground(new java.awt.Color(0, 0, 0));
        jLabel7.setText("Color");

        jLabel8.setForeground(new java.awt.Color(0, 0, 0));
        jLabel8.setText("Chassis");

        jLabel9.setForeground(new java.awt.Color(0, 0, 0));
        jLabel9.setText("Car Plate");

        jLabel10.setForeground(new java.awt.Color(0, 0, 0));
        jLabel10.setText("VIN");

        YearTextField.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                YearTextFieldKeyTyped(evt);
            }
        });

        jLabel11.setForeground(new java.awt.Color(0, 0, 0));
        jLabel11.setText("Fuel Type");

        jLabel12.setForeground(new java.awt.Color(0, 0, 0));
        jLabel12.setText("Type Of Use");

        jLabel13.setForeground(new java.awt.Color(0, 0, 0));
        jLabel13.setText("Service");

        jLabel14.setForeground(new java.awt.Color(0, 0, 0));
        jLabel14.setText("Gravamen");

        jLabel15.setForeground(new java.awt.Color(0, 0, 0));
        jLabel15.setText("Num. Pas");

        jLabel16.setForeground(new java.awt.Color(0, 0, 0));
        jLabel16.setText("Tonnage");

        jLabel18.setForeground(new java.awt.Color(0, 0, 0));
        jLabel18.setText("Num. Cilinder");

        NumPassTextField.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                NumPassTextFieldKeyTyped(evt);
            }
        });

        TonnageTextField.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                TonnageTextFieldKeyTyped(evt);
            }
        });

        NumCilinderTextField.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                NumCilinderTextFieldKeyTyped(evt);
            }
        });

        jLabel19.setForeground(new java.awt.Color(0, 0, 0));
        jLabel19.setText("Date Of Issue");

        try {
            DateOfIssuesTextField.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.MaskFormatter("##/##/####")));
        } catch (java.text.ParseException ex) {
            ex.printStackTrace();
        }
        DateOfIssuesTextField.setHorizontalAlignment(javax.swing.JTextField.CENTER);

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addGap(14, 14, 14)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addComponent(jLabel2)
                        .addGap(18, 18, 18)
                        .addComponent(BrandTextField, javax.swing.GroupLayout.PREFERRED_SIZE, 102, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel5)
                            .addComponent(jLabel6)
                            .addComponent(jLabel7))
                        .addGap(18, 18, 18)
                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(ColorTextField)
                            .addComponent(ModelTextField)
                            .addComponent(YearTextField)))
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addComponent(jLabel8)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(ChassisTextField, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(27, 27, 27)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addComponent(jLabel10)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(VinTextField, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addComponent(jLabel9)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(PlacaTextField, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel3Layout.createSequentialGroup()
                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel16)
                            .addComponent(jLabel15)
                            .addComponent(jLabel14))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(GravamenComboBox, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(NumPassTextField, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(TonnageTextField, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addGap(18, 18, 18)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(jLabel13)
                            .addComponent(jLabel12)
                            .addComponent(jLabel11)
                            .addComponent(jLabel18))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(TypeOfUseComboBox, javax.swing.GroupLayout.Alignment.TRAILING, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addGroup(jPanel3Layout.createSequentialGroup()
                                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addComponent(NumCilinderTextField, javax.swing.GroupLayout.DEFAULT_SIZE, 100, Short.MAX_VALUE)
                                    .addComponent(FuelTypeComboBox, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                                .addGap(0, 0, Short.MAX_VALUE))
                            .addComponent(ServiceComboBox, javax.swing.GroupLayout.Alignment.TRAILING, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addComponent(jLabel19)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(DateOfIssuesTextField, javax.swing.GroupLayout.PREFERRED_SIZE, 104, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2)
                    .addComponent(BrandTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel18)
                    .addComponent(jLabel10)
                    .addComponent(VinTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(NumCilinderTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(ModelTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel5)
                    .addComponent(jLabel11)
                    .addComponent(jLabel9)
                    .addComponent(PlacaTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(FuelTypeComboBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(YearTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel6)
                    .addComponent(jLabel12)
                    .addComponent(jLabel14)
                    .addComponent(GravamenComboBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(TypeOfUseComboBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(ColorTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel7)
                    .addComponent(jLabel13)
                    .addComponent(jLabel15)
                    .addComponent(NumPassTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(ServiceComboBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(ChassisTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel8)
                    .addComponent(jLabel16)
                    .addComponent(TonnageTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel19)
                    .addComponent(DateOfIssuesTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(0, 0, Short.MAX_VALUE))
        );

        SaveButton.setForeground(new java.awt.Color(0, 0, 0));
        SaveButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Resources/Save.png"))); // NOI18N
        SaveButton.setText("Save");
        SaveButton.setBorder(null);
        SaveButton.setBorderPainted(false);

        CleanButton.setForeground(new java.awt.Color(0, 0, 0));
        CleanButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Resources/Clean.png"))); // NOI18N
        CleanButton.setText("Clean");
        CleanButton.setBorder(null);
        CleanButton.setBorderPainted(false);

        SelectButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Resources/Select.png"))); // NOI18N
        SelectButton.setText("Select");
        SelectButton.setBorder(null);
        SelectButton.setBorderPainted(false);

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGap(33, 33, 33)
                .addComponent(SaveButton, javax.swing.GroupLayout.PREFERRED_SIZE, 80, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(CleanButton, javax.swing.GroupLayout.PREFERRED_SIZE, 80, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(118, 118, 118)
                .addComponent(SelectButton, javax.swing.GroupLayout.PREFERRED_SIZE, 80, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(50, 50, 50))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(7, 7, 7)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(SaveButton, javax.swing.GroupLayout.PREFERRED_SIZE, 43, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(CleanButton, javax.swing.GroupLayout.PREFERRED_SIZE, 43, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(SelectButton, javax.swing.GroupLayout.PREFERRED_SIZE, 43, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        getContentPane().add(jPanel2, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 70, 590, 250));

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void YearTextFieldKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_YearTextFieldKeyTyped
        int i = evt.getKeyChar();
        if ((i < '0' || i > '9')) {
            Toolkit.getDefaultToolkit().beep();
            evt.consume();
        }
    }//GEN-LAST:event_YearTextFieldKeyTyped

    private void NumPassTextFieldKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_NumPassTextFieldKeyTyped
        int i = evt.getKeyChar();
        if ((i < '0' || i > '9')) {
            Toolkit.getDefaultToolkit().beep();
            evt.consume();
        }
    }//GEN-LAST:event_NumPassTextFieldKeyTyped

    private void TonnageTextFieldKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_TonnageTextFieldKeyTyped
        int i = evt.getKeyChar();
        if ((i < '0' || i > '9')) {
            Toolkit.getDefaultToolkit().beep();
            evt.consume();
        }
    }//GEN-LAST:event_TonnageTextFieldKeyTyped

    private void NumCilinderTextFieldKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_NumCilinderTextFieldKeyTyped
        int i = evt.getKeyChar();
        if ((i < '0' || i > '9')) {
            Toolkit.getDefaultToolkit().beep();
            evt.consume();
        }
    }//GEN-LAST:event_NumCilinderTextFieldKeyTyped


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JTextField BrandTextField;
    private javax.swing.JTextField ChassisTextField;
    private javax.swing.JButton CleanButton;
    private javax.swing.JTextField ColorTextField;
    private javax.swing.JFormattedTextField DateOfIssuesTextField;
    private javax.swing.JComboBox<String> FuelTypeComboBox;
    private javax.swing.JComboBox<String> GravamenComboBox;
    private javax.swing.JTextField ModelTextField;
    private javax.swing.JTextField NumCilinderTextField;
    private javax.swing.JTextField NumPassTextField;
    private javax.swing.JTextField PlacaTextField;
    private javax.swing.JButton SaveButton;
    private javax.swing.JButton SelectButton;
    private javax.swing.JComboBox<String> ServiceComboBox;
    private javax.swing.JTextField TonnageTextField;
    private javax.swing.JComboBox<String> TypeOfUseComboBox;
    private javax.swing.JTextField VinTextField;
    private javax.swing.JTextField YearTextField;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel15;
    private javax.swing.JLabel jLabel16;
    private javax.swing.JLabel jLabel18;
    private javax.swing.JLabel jLabel19;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    // End of variables declaration//GEN-END:variables
}
